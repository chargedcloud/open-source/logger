<div align="center">

# @chargedcloud/logger

Logger is a module to help you log your messages to the console of your **NodeJS** application.

[Installation](#installation) • [Usage](#usage) • [Methods](#methods)

</div>

## Installation

In your project directory, run the following command:

```bash
npm install @chargedcloud/logger
```

## Usage

First, for you create an instance of the logger, you need to import the module:

```js
// CommonJS
const createLogger = require("@chargedcloud/logger");

// ES6
import createLogger from "@chargedcloud/logger";
```

Then, you can create an instance of the logger:

```js
const logger = createLogger(import.meta.url);
```
