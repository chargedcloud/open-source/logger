/* eslint-disable no-restricted-syntax */
import logNode from 'log-node';
import log from 'log';

logNode();

export default function createLogger(importmeta) {
  const absolutePath = importmeta.replace(`file://${process.cwd()}/src/`, '').replace('.mjs', '');

  const fileName = absolutePath.split('/').slice(-2).join(':').toLowerCase();

  return log.get(fileName);
}

export function withLogger(controller, logger) {
  const controllerWithLog = {};
  for (const functionName in controller) {
    if (Object.hasOwnProperty.call(controller, functionName)) {
      const func = controller[functionName];
      const newLogger = logger.get(functionName.toLowerCase());
      controllerWithLog[functionName] = (req, res) => func(req, res, newLogger);
    }
  }
  return controllerWithLog;
}
